package pages;

import com.codeborne.selenide.Condition;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

import static com.codeborne.selenide.Selenide.$;


public class AttractionProfilePage {
    WebDriver driver;
    public final By ATTRACTION_NAME = By.xpath("//a[@aria-current='location']");
    public final By LANGUAGE_BUTTON = By.xpath("//button[@data-testid='header-language-picker-trigger']");


    public AttractionProfilePage changeLanguage(){
        $(LANGUAGE_BUTTON).click();
        return this;
    }

    public AttractionProfilePage checkLabel() {
        $(ATTRACTION_NAME).should(Condition.exist);
        return this;
    }
}
