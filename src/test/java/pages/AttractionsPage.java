package pages;

import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;

public class AttractionsPage {
    public final By PLACE = By.xpath("//input[@name='query']");
    public final By SELECT_PLACE = By.xpath("//a[@country='fr' and @city='paris']");
    public final By DATE = By.xpath("//div[@class='css-ck8kih']");
    public final By DATE_BEGIN = By.xpath("//span[@data-date='2023-05-01']");
    public final By DATE_END = By.xpath("//span[@data-date='2023-05-12']");
    public final By SUBMIT_BUTTON = By.xpath("//button[@type='submit']");
    public AttractionsPage selectCity(String city){
        $(PLACE).sendKeys(city);
        $(SELECT_PLACE).click();
        return this;
    }
    public AttractionsPage selectDate(){
        $(DATE).click();
        $(DATE_BEGIN).click();
        $(DATE_END).click();
        return this;
    }
    public AttractionsPage submit(){
        $(SUBMIT_BUTTON).click();
        return this;
    }


}
