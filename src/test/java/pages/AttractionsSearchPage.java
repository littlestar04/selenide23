package pages;

import com.codeborne.selenide.Condition;
import org.junit.Assert;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;

public class AttractionsSearchPage {
    public final By DURATION = By.xpath("//div[@class='db29ecfbe2 css-j786b1']");
    public final By ATTRACTION = By.xpath("//div[@class='css-12m1zyc']");
    public final By RETURN = By.xpath("//div[@class='db29ecfbe2 cf9ebde7b2 css-j786b1']");
    public final By CITY_NAME = By.xpath("//h1");


    public AttractionsSearchPage checkReturn(){
        Assert.assertTrue($(RETURN).getText().contains("Доступна бесплатная отмена"));
        return this;
    }
    public AttractionsSearchPage checkDuration(){
        Assert.assertTrue($(DURATION).getText().contains("60"));
        return this;
    }
    public AttractionsSearchPage checkCity(){
        $(CITY_NAME).shouldHave(Condition.text("Достопримечательности города Париж"));
        //Assert.assertTrue($(CITY_NAME).getText().contains("Достопримечательности города Париж"));
        return this;
    }
    public AttractionsSearchPage selectAttraction(){
        $(ATTRACTION).click();
        return this;
    }
}
