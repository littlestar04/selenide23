package pages;


import com.codeborne.selenide.Condition;
import com.codeborne.selenide.Selenide;
import config.ProjectConfig;
import org.aeonbits.owner.ConfigFactory;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;

public class HomePage {
    public static final ProjectConfig config = ConfigFactory.create(ProjectConfig.class);
    private final By SEARCH_CITY = By.xpath("//input[@id=':Ra9:']");
    private final By SUBMIT_BUTTON = By.xpath("//button[@type='submit']");
    private final By ACCEPT_COOKIES_BUTTON = By.xpath("//button[@id='onetrust-accept-btn-handler']");
    private final By DROP_DOWN_LIST_ITEMS = By.xpath("//div[@class='a40619bfbe']");
    private final By DATES = By.xpath("//div[@class='d606c76c5a']");
    private final By DATE_FROM = By.xpath("//span[@data-date='2023-04-21']");
    private final By DATE_TO = By.xpath("//span[@data-date='2023-05-19']");
    private final By ENTERTAINMENTS = By.xpath("//a[@id='attractions']");



    public HomePage openHomePage(){
        Selenide.open(config.base_url());
        return this;
    }

    public HomePage searchCity(String city){
        $(SEARCH_CITY).sendKeys(city);
        $$(DROP_DOWN_LIST_ITEMS).get(0).shouldHave(Condition.text(city));
        $(SUBMIT_BUTTON).click();
        return this;
    }
    public HomePage chooseCity(String city){
        $(SEARCH_CITY).sendKeys(city);
        $$(DROP_DOWN_LIST_ITEMS).get(0).shouldHave(Condition.text(city));
        return this;
    }

    public HomePage acceptCookies() {
        $(ACCEPT_COOKIES_BUTTON).click();
        return this;
    }

    public HomePage chooseDate(){
        $(DATES).click();
        $(DATE_FROM).click();
        $(DATE_TO).click();
        $(SUBMIT_BUTTON).click();
        return this;
    }

    public HomePage attractionsSection(){
        $(ENTERTAINMENTS).click();
        return this;
    }
}
