package pages;

import org.openqa.selenium.By;

import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Selenide.$;

public class HotelProfilePage extends HotelSearchPage {

    public final By HOTEL_NAME2 = By.xpath("//h2[@class='d2fee87262 pp-header__title'] | //a[@id='hp_hotel_name_reviews'] | //div[@id='hp_hotel_name']//h2");
    public final By HOTEL_PRICE = By.xpath("//span[@class='prco-valign-middle-helper']");
    public final By REVIEWS = By.xpath("//div[@class='d8eab2cf7f c90c0a70d3 db63693c62']");
    public final By RATING = By.xpath("//div[@class='b5cd09854e d10a6220b4']");

// "//div[@data-testid='review-score-right-component'] |

    public HotelProfilePage checkPrice(String mapValue){
        $(HOTEL_PRICE).shouldHave(text(mapValue));
        return this;
    }

    public HotelProfilePage checkReviews(String mapValue){
        $(REVIEWS).shouldHave(text(mapValue));
        return this;
    }

    public HotelProfilePage checkName(String mapValue){
        $(HOTEL_NAME2).shouldHave(text(mapValue));
        return this;
    }

    public HotelProfilePage checkRating(String mapValue){
        $(RATING).shouldHave(text(mapValue));
        return this;
    }



}
