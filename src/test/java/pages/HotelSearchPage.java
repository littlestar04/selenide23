package pages;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.ElementsCollection;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;


import java.time.Duration;

import static com.codeborne.selenide.Selenide.$;

public class HotelSearchPage {
    public final By CITY_HEADER = By.xpath("//h1");
    public final By SHOW_MAP = By.xpath("//div[@data-testid='map-trigger']");
    public final By HOTEL = By.xpath("//a[@data-testid='property-list-map-card'] | //div[@class='map-card__content-container']");
    public final By HOTEL_MARKER = By.xpath("//div[contains(@class, 'bounce')]");
    public final By HOTEL_NAME = By.xpath("//span[@data-testid='header-title']");


    private WebDriver driver;
    private String hotelId;
    public String hotelNameFromMap;
    public ElementsCollection mapHotelStars;
    public String mapHotelAverageRating;
    public String mapHotelReviews;
    public String mapHotelPrice;

    public HotelSearchPage getHotelPrice(){
        //mapHotelPrice = $(By.cssSelector(".prco-valign-middle-helper")).getText();
        mapHotelPrice = String.valueOf($(By
                .xpath("//span[@class='fcab3ed991 bd73d13072'] | //span[@class='prco-valign-middle-helper']")).getText());
        return this;
    }


    //mapHotelReviews = $(By.cssSelector(".bui-review-score__text ")).getText();
    public HotelSearchPage getHotelReviews(){
        mapHotelReviews = $(By.xpath("//div[@class='d8eab2cf7f c90c0a70d3 db63693c62']")).getText();
        return this;
    }
    public HotelSearchPage getHotelName(){
        hotelNameFromMap = $(By.xpath("//span[@data-testid='header-title'] | //span[@class='map-card__title-link']")).getText();
        return this;
    }

    public HotelSearchPage getHotelRating(){
        mapHotelAverageRating = $(By
                .xpath("//div[@class='a1b3f50dcd a1f3ecff04 b2fe1a41c3 db7f07f643 d19ba76520 d17b3fe5e2']//child::div[@aria-label] | //div[@class='bui-review-score__badge']")).getText();
        return this;
    }

    public HotelSearchPage seeOnMap(){
        $(SHOW_MAP).click();
        return this;
    }
    public HotelSearchPage seeHotel(){
        $(HOTEL).shouldBe(Condition.exist,Duration.ofSeconds(30)).hover();
        return this;
    }


    public HotelSearchPage goToHotelProfile(){
        $(HOTEL_MARKER).shouldBe(Condition.exist,Duration.ofSeconds(10)).click();
        return this;
    }




}
