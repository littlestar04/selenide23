package tests;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.WebDriverRunner;
import org.junit.Test;
import pages.AttractionProfilePage;
import pages.AttractionsPage;
import pages.AttractionsSearchPage;
import pages.HomePage;

public class ActivityCheckTest extends BaseTest{
    HomePage homePage = new HomePage();
    private String cityName = "Париж";
    AttractionsPage attractionsPage = new AttractionsPage();
    AttractionsSearchPage attractionsSearchPage = new AttractionsSearchPage();
    AttractionProfilePage attractionProfilePage = new AttractionProfilePage();

    @Test
    public void checkActivities(){
        homePage.openHomePage()
                .acceptCookies()
                .attractionsSection();
        attractionsPage.selectCity(cityName)
                .selectDate()
                .submit();
        attractionsSearchPage.checkCity()
                .checkReturn()
                .checkDuration()
                .selectAttraction();
        attractionProfilePage.checkLabel();
        Configuration.holdBrowserOpen = true;

    }
}
