package tests;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.Selenide;
import org.junit.Test;
import pages.HomePage;
import pages.HotelProfilePage;
import pages.HotelSearchPage;

public class HotelCheckTest extends BaseTest {
    HomePage homePage = new HomePage();
    HotelSearchPage hotelSearchPage = new HotelSearchPage();
    HotelProfilePage hotelProfilePage = new HotelProfilePage();
    private String cityName = "Париж";


    @Test
    public void bookingCheckHotel(){
        Selenide.open(config.base_url());
        homePage.acceptCookies()
                .chooseCity(cityName).chooseDate();
        hotelSearchPage.seeOnMap()
                .seeHotel()
                .getHotelName()
                .getHotelPrice()
                .getHotelRating()
                .getHotelReviews();
        String mapHotelAverageRating = hotelSearchPage.mapHotelAverageRating;
        String hotelNameFromMap = hotelSearchPage.hotelNameFromMap;
        String mapHotelPrice = hotelSearchPage.mapHotelPrice;
        String mapHotelReviews = hotelSearchPage.mapHotelReviews;
        hotelSearchPage.goToHotelProfile();
        hotelProfilePage.checkPrice(mapHotelPrice);
        hotelProfilePage.checkReviews(mapHotelReviews);
        hotelProfilePage.checkRating(mapHotelAverageRating);
        //hotelProfilePage.checkName(hotelNameFromMap);


        Configuration.holdBrowserOpen = true;

    }
}
